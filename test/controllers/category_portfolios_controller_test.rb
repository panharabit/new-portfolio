require 'test_helper'

class CategoryPortfoliosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @category_portfolio = category_portfolios(:one)
  end

  test "should get index" do
    get category_portfolios_url
    assert_response :success
  end

  test "should get new" do
    get new_category_portfolio_url
    assert_response :success
  end

  test "should create category_portfolio" do
    assert_difference('CategoryPortfolio.count') do
      post category_portfolios_url, params: { category_portfolio: { category_name: @category_portfolio.category_name } }
    end

    assert_redirected_to category_portfolio_url(CategoryPortfolio.last)
  end

  test "should show category_portfolio" do
    get category_portfolio_url(@category_portfolio)
    assert_response :success
  end

  test "should get edit" do
    get edit_category_portfolio_url(@category_portfolio)
    assert_response :success
  end

  test "should update category_portfolio" do
    patch category_portfolio_url(@category_portfolio), params: { category_portfolio: { category_name: @category_portfolio.category_name } }
    assert_redirected_to category_portfolio_url(@category_portfolio)
  end

  test "should destroy category_portfolio" do
    assert_difference('CategoryPortfolio.count', -1) do
      delete category_portfolio_url(@category_portfolio)
    end

    assert_redirected_to category_portfolios_url
  end
end
