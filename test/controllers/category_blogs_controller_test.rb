require 'test_helper'

class CategoryBlogsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @category_blog = category_blogs(:one)
  end

  test "should get index" do
    get category_blogs_url
    assert_response :success
  end

  test "should get new" do
    get new_category_blog_url
    assert_response :success
  end

  test "should create category_blog" do
    assert_difference('CategoryBlog.count') do
      post category_blogs_url, params: { category_blog: { category_name: @category_blog.category_name } }
    end

    assert_redirected_to category_blog_url(CategoryBlog.last)
  end

  test "should show category_blog" do
    get category_blog_url(@category_blog)
    assert_response :success
  end

  test "should get edit" do
    get edit_category_blog_url(@category_blog)
    assert_response :success
  end

  test "should update category_blog" do
    patch category_blog_url(@category_blog), params: { category_blog: { category_name: @category_blog.category_name } }
    assert_redirected_to category_blog_url(@category_blog)
  end

  test "should destroy category_blog" do
    assert_difference('CategoryBlog.count', -1) do
      delete category_blog_url(@category_blog)
    end

    assert_redirected_to category_blogs_url
  end
end
