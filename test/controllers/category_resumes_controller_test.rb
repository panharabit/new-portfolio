require 'test_helper'

class CategoryResumesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @category_resume = category_resumes(:one)
  end

  test "should get index" do
    get category_resumes_url
    assert_response :success
  end

  test "should get new" do
    get new_category_resume_url
    assert_response :success
  end

  test "should create category_resume" do
    assert_difference('CategoryResume.count') do
      post category_resumes_url, params: { category_resume: { category_name: @category_resume.category_name, resume_id: @category_resume.resume_id } }
    end

    assert_redirected_to category_resume_url(CategoryResume.last)
  end

  test "should show category_resume" do
    get category_resume_url(@category_resume)
    assert_response :success
  end

  test "should get edit" do
    get edit_category_resume_url(@category_resume)
    assert_response :success
  end

  test "should update category_resume" do
    patch category_resume_url(@category_resume), params: { category_resume: { category_name: @category_resume.category_name, resume_id: @category_resume.resume_id } }
    assert_redirected_to category_resume_url(@category_resume)
  end

  test "should destroy category_resume" do
    assert_difference('CategoryResume.count', -1) do
      delete category_resume_url(@category_resume)
    end

    assert_redirected_to category_resumes_url
  end
end
