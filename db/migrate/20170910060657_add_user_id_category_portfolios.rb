class AddUserIdCategoryPortfolios < ActiveRecord::Migration[5.1]
  def change
    add_column :category_portfolios, :user_id, :integer
  end
end
