class CreateCateBlogs < ActiveRecord::Migration[5.1]
  def change
    create_table :cate_blogs do |t|
      t.string :category_name

      t.timestamps
    end
  end
end
