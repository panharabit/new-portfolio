class CreateResumes < ActiveRecord::Migration[5.1]
  def change
    create_table :resumes do |t|
      t.string :title
      t.string :position
      t.datetime :start_time
      t.datetime :current_time
      t.string :image
      t.integer :category_resume_id
      t.text :description
      t.timestamps
    end
  end
end
