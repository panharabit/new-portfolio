class CreateCategoryPortfolios < ActiveRecord::Migration[5.1]
  def change
    create_table :category_portfolios do |t|
      t.string :category_name

      t.timestamps
    end
  end
end
