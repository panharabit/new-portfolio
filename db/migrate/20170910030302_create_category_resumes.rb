class CreateCategoryResumes < ActiveRecord::Migration[5.1]
  def change
    create_table :category_resumes do |t|
      t.string :category_name
      t.timestamps
    end
  end
end
