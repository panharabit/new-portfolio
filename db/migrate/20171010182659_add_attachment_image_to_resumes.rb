class AddAttachmentImageToResumes < ActiveRecord::Migration[5.1]
  def self.up
    change_table :resumes do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :resumes, :image
  end
end
