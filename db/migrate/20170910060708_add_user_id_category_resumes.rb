class AddUserIdCategoryResumes < ActiveRecord::Migration[5.1]
  def change
    add_column :category_resumes, :user_id, :integer
  end
end
