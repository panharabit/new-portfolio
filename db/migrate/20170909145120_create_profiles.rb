class CreateProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :profiles do |t|
      t.string :first_name
      t.string :last_name
      t.integer :age
      t.string :job_title
      t.string :location
      t.text :description
      t.string :image
      t.string :cv
      t.string :phone
      t.string :email
      t.string :address
      t.integer :profile_id

      t.timestamps
    end
  end
end
