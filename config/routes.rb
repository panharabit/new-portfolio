Rails.application.routes.draw do

  scope :administrator do
    resources :cate_blogs
    resources :blogs
    resources :teams
    resources :services
    resources :portfolios
    resources :category_portfolios
    resources :resumes
    resources :category_resumes
    resources :skills
    resources :profiles
    devise_for :users
    resources :users
    get "dashboard" => 'dashboards#index', :as => :dashboard_page
  end

  resources :news
  root "homes#index"


end
