class HomesController < NewBootstrapController


  def index
    @banners = Skill.all
    @about = Profile.all
    @skills = Skill.all
    @category_resumes = CategoryResume.all.limit(3)
    @experience = Resume.where({category_resume_id:1})
    @education = Resume.where({category_resume_id:2})
    @award = Resume.where({category_resume_id:3})
    @services = Service.all
    @category_portfolios = CategoryPortfolio.all
    @portfolios = Portfolio.all
    @blogs = Blog.all.limit(6)
    @recent = Blog.order('created_at DESC').limit(5)
  end


end
