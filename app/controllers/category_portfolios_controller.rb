class CategoryPortfoliosController < ApplicationController
  before_action :set_category_portfolio, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:index,:show,:new,:edit,:update,:destroy]
  before_action :admin_only, only: [:new,:edit,:update,:destroy]
  # GET /category_portfolios
  # GET /category_portfolios.json
  def index
    @category_portfolios = CategoryPortfolio.all
    @fields = ["ID","Category"]
  end

  # GET /category_portfolios/1
  # GET /category_portfolios/1.json
  def show
  end

  # GET /category_portfolios/new
  def new
    @category_portfolio = current_user.category_portfolios.build
  end

  # GET /category_portfolios/1/edit
  def edit
  end

  # POST /category_portfolios
  # POST /category_portfolios.json
  def create
    @category_portfolio = current_user.category_portfolios.build(category_portfolio_params)

    respond_to do |format|
      if @category_portfolio.save
        format.html { redirect_to @category_portfolio, notice: 'Category portfolio was successfully created.' }
        format.json { render :show, status: :created, location: @category_portfolio }
      else
        format.html { render :new }
        format.json { render json: @category_portfolio.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /category_portfolios/1
  # PATCH/PUT /category_portfolios/1.json
  def update
    respond_to do |format|
      if @category_portfolio.update(category_portfolio_params)
        format.html { redirect_to @category_portfolio, notice: 'Category portfolio was successfully updated.' }
        format.json { render :show, status: :ok, location: @category_portfolio }
      else
        format.html { render :edit }
        format.json { render json: @category_portfolio.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /category_portfolios/1
  # DELETE /category_portfolios/1.json
  def destroy
    @category_portfolio.destroy
    respond_to do |format|
      format.html { redirect_to category_portfolios_url, notice: 'Category portfolio was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category_portfolio
      @category_portfolio = CategoryPortfolio.find(params[:id])
    end

    def admin_only
      unless current_user.admin?
        redirect_to root_path, :alert => "Access denied."
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_portfolio_params
      params.require(:category_portfolio).permit(:category_name)
    end
end
