class CategoryResumesController < ApplicationController
  before_action :set_category_resume, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:index,:show,:new,:edit,:update,:destroy]
  before_action :admin_only, only: [:new,:edit,:update,:destroy]
  # GET /category_resumes
  # GET /category_resumes.json
  def index
    @category_resumes = CategoryResume.all
    @fields = ["ID","Category"]
  end

  # GET /category_resumes/1
  # GET /category_resumes/1.json
  def show
  end

  # GET /category_resumes/new
  def new
    @category_resume = current_user.category_resumes.build
  end

  # GET /category_resumes/1/edit
  def edit
  end

  # POST /category_resumes
  # POST /category_resumes.json
  def create
    @category_resume = current_user.category_resumes.build(category_resume_params)

    respond_to do |format|
      if @category_resume.save
        format.html { redirect_to @category_resume, notice: 'Category resume was successfully created.' }
        format.json { render :show, status: :created, location: @category_resume }
      else
        format.html { render :new }
        format.json { render json: @category_resume.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /category_resumes/1
  # PATCH/PUT /category_resumes/1.json
  def update
    respond_to do |format|
      if @category_resume.update(category_resume_params)
        format.html { redirect_to @category_resume, notice: 'Category resume was successfully updated.' }
        format.json { render :show, status: :ok, location: @category_resume }
      else
        format.html { render :edit }
        format.json { render json: @category_resume.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /category_resumes/1
  # DELETE /category_resumes/1.json
  def destroy
    @category_resume.destroy
    respond_to do |format|
      format.html { redirect_to category_resumes_url, notice: 'Category resume was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_category_resume
      @category_resume = CategoryResume.find(params[:id])
    end

    def admin_only
      unless current_user.admin?
        redirect_to root_path, :alert => "Access denied."
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def category_resume_params
      params.require(:category_resume).permit(:category_name, :resume_id)
    end
end
