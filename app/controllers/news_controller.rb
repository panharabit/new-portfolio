class NewsController < NewBootstrapController
  before_action :set_news, only: [:show]
  def index
    @blogs = Blog.all
    @recent = Blog.order('created_at DESC').limit(5)
    @category = CateBlog.all.limit(5)
  end

  def show
  end


  private
    def set_news
      @blog = Blog.find_by_slug(params[:id])
    end
end
