class CateBlogsController < ApplicationController
  before_action :set_cate_blog, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:index,:show,:new,:edit,:update,:destroy]
  before_action :admin_only, only: [:new,:edit,:update,:destroy]

  # GET /cate_blogs
  # GET /cate_blogs.json
  def index
    @cate_blogs = CateBlog.all
    @fields = ["ID","Category"]
  end

  # GET /cate_blogs/1
  # GET /cate_blogs/1.json
  def show
  end

  # GET /cate_blogs/new
  def new
    @cate_blog = current_user.cate_blogs.build
  end

  # GET /cate_blogs/1/edit
  def edit
  end

  # POST /cate_blogs
  # POST /cate_blogs.json
  def create
    @cate_blog = current_user.cate_blogs.build(cate_blog_params)

    respond_to do |format|
      if @cate_blog.save
        format.html { redirect_to @cate_blog, notice: 'Cate blog was successfully created.' }
        format.json { render :show, status: :created, location: @cate_blog }
      else
        format.html { render :new }
        format.json { render json: @cate_blog.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cate_blogs/1
  # PATCH/PUT /cate_blogs/1.json
  def update
    respond_to do |format|
      if @cate_blog.update(cate_blog_params)
        format.html { redirect_to @cate_blog, notice: 'Cate blog was successfully updated.' }
        format.json { render :show, status: :ok, location: @cate_blog }
      else
        format.html { render :edit }
        format.json { render json: @cate_blog.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cate_blogs/1
  # DELETE /cate_blogs/1.json
  def destroy
    @cate_blog.destroy
    respond_to do |format|
      format.html { redirect_to cate_blogs_url, notice: 'Cate blog was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cate_blog
      @cate_blog = CateBlog.find(params[:id])
    end
    
    def admin_only
      unless current_user.admin?
        redirect_to root_path, :alert => "Access denied."
      end
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def cate_blog_params
      params.require(:cate_blog).permit(:category_name)
    end
end
