class Profile < ApplicationRecord
  has_many :skills, inverse_of: :profile
  belongs_to :user
  accepts_nested_attributes_for :skills, reject_if: :all_blank, allow_destroy: true
  has_attached_file :image, style: { medium: "300x500>",thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
end
