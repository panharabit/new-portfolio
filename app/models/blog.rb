class Blog < ApplicationRecord
  before_save :update_slug
  #Associations
  belongs_to :cate_blog
  belongs_to :user

  #validation
  has_attached_file :image, style: { medium: "300x500>",thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  #instace method
  def update_slug
    self.slug = title.parameterize
  end

  def to_param
    slug
  end
end
