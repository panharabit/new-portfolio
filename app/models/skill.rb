class Skill < ApplicationRecord
  belongs_to :profile, required: false
  belongs_to :user,required: false
end
