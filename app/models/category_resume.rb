class CategoryResume < ApplicationRecord
  has_many :resumes
  belongs_to :user
end
