class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # Association
  has_many :profiles
  has_many :category_resumes
  has_many :category_portfolios
  has_many :portfolios
  has_many :resumes
  has_many :services
  has_many :skills
  has_many :teams
  has_many :blogs
  has_many :cate_blogs
  #Devise
  enum role: [:user, :vip, :admin]
  after_initialize :set_default_role, :if => :new_record?

  devise :database_authenticatable, :registerable,
  :recoverable, :rememberable, :trackable, :validatable

  #Validation
  has_attached_file :image, style: { medium: "300x500>",thumb: "100x100>" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  #instance method
  def set_default_role
    self.role ||= :user
  end

  def full_name
    "#{first_name}" "#{last_name}"
  end
end
