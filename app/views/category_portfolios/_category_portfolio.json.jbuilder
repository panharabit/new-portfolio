json.extract! category_portfolio, :id, :category_name, :created_at, :updated_at
json.url category_portfolio_url(category_portfolio, format: :json)
