json.extract! portfolio, :id, :description, :tool, :image, :category_portfolio_id, :created_at, :updated_at
json.url portfolio_url(portfolio, format: :json)
