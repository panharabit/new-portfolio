json.extract! service, :id, :title, :fa_icon, :description, :created_at, :updated_at
json.url service_url(service, format: :json)
