json.extract! skill, :id, :skill_name, :percentage, :profile_id, :created_at, :updated_at
json.url skill_url(skill, format: :json)
