json.extract! category_resume, :id, :category_name, :resume_id, :created_at, :updated_at
json.url category_resume_url(category_resume, format: :json)
