json.extract! team, :id, :first_name, :last_name, :position, :user_id, :created_at, :updated_at
json.url team_url(team, format: :json)
