json.extract! resume, :id, :title, :position, :start_time, :current_time, :image, :category_resume_id, :created_at, :updated_at
json.url resume_url(resume, format: :json)
