json.extract! profile, :id, :first_name, :last_name, :age, :job_title, :location, :description, :image, :cv, :phone, :email, :address, :created_at, :updated_at
json.url profile_url(profile, format: :json)
